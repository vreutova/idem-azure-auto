Idem Commands
=============

Idem has many built-in commands. In addition, Idem can be easily extended with
plugins.

We're going to focus on two main functions :doc:`Describe</quickstart/commands/describe>` and :doc:`State</quickstart/commands/state>`

.. grid:: 1

    .. grid-item-card:: Describe
        :link: describe
        :link-type: doc

        Use the **Describe** command to get details about your environment.

        :bdg-info:`Describe`

    .. grid-item-card:: State
        :link: state
        :link-type: doc

        Use the **State** command to enforce your configuration on your
        environment.

        :bdg-info:`State`


.. toctree::
   :maxdepth: 3
   :hidden:

   describe
   state
   examples
