Configure idem-azure
====================

Credential management in Idem is handled by the Idem  **acct** module. Details
about this module can be found on the `acct gitlab repo README
<https://gitlab.com/vmware/idem/acct>`_.
