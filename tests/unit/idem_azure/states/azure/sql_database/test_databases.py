import copy
from collections import ChainMap

import pytest
from dict_tools import differ

RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
SERVER_NAME = "my-server"
DATABASE_NAME = "my-sqldb"
SUBSCRIPTION_ID = "11111111-2222-3333-4444-555555555555"
RESOURCE_PARAMETERS = {
    "location": "westeurope",
    "subscription_id": SUBSCRIPTION_ID,
    "sku": {
        "name": "Standard",
        "tier": "Standard",
        "capacity": 10,
    },
    "collation": "SQL_Latin1_General_CP1_CI_AS",
    "max_size_bytes": 268435456000,
    "read_scale": "Disabled",
    "requested_backup_storage_redundancy": "Local",
    "tags": {"tag-key": "tag-value"},
}
RESOURCE_PARAMETERS_RAW = {
    "location": "westeurope",
    "properties": {
        "collation": "SQL_Latin1_General_CP1_CI_AS",
        "maxSizeBytes": 268435456000,
        "readScale": "Disabled",
        "requestedBackupStorageRedundancy": "Local",
    },
    "sku": {
        "name": "Standard",
        "tier": "Standard",
        "capacity": 10,
    },
    "tags": {"tag-key": "tag-value"},
}


@pytest.fixture(autouse=True)
async def update_tag(hub, mock_hub, ctx):
    # 'fake_|-test_|-tag'
    ctx["tag"] = "azure.sql_database.databases_|-test_|-tag"


@pytest.mark.skip(reason="Test will be disabled until module is refactored")
@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of virtual networks. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.sql_database.databases.present = (
        hub.states.azure.sql_database.databases.present
    )
    mock_hub.tool.azure.sql_database.databases.convert_raw_database_to_present = (
        hub.tool.azure.sql_database.databases.convert_raw_database_to_present
    )
    mock_hub.tool.azure.sql_database.databases.convert_raw_sku_to_present = (
        hub.tool.azure.sql_database.databases.convert_raw_sku_to_present
    )
    mock_hub.tool.azure.sql_database.databases.convert_raw_identity_to_present = (
        hub.tool.azure.sql_database.databases.convert_raw_identity_to_present
    )
    mock_hub.tool.azure.sql_database.databases.convert_present_to_raw_database = (
        hub.tool.azure.sql_database.databases.convert_present_to_raw_database
    )
    mock_hub.tool.azure.sql_database.databases.convert_present_to_raw_sku = (
        hub.tool.azure.sql_database.databases.convert_present_to_raw_sku
    )
    mock_hub.tool.azure.sql_database.databases.convert_present_to_raw_identity = (
        hub.tool.azure.sql_database.databases.convert_present_to_raw_identity
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.sql_database.databases.get = (
        hub.exec.azure.sql_database.databases.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.resource_utils.construct_resource_id = (
        hub.tool.azure.resource_utils.construct_resource_id
    )
    mock_hub.tool.azure.resource_utils.construct_resource_url = (
        hub.tool.azure.resource_utils.construct_resource_url
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}/providers/Microsoft.Sql/servers/{SERVER_NAME}/databases/{DATABASE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert SERVER_NAME in url
        assert DATABASE_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert SERVER_NAME in url
        assert DATABASE_NAME in url
        assert not differ.deep_diff(RESOURCE_PARAMETERS_RAW, json)
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.sql_database.databases.present(
        test_ctx,
        **{
            "name": RESOURCE_NAME,
            "resource_group_name": RESOURCE_GROUP_NAME,
            "server_name": SERVER_NAME,
            "database_name": DATABASE_NAME,
        },
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.sql_database.databases '{RESOURCE_NAME}'" in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.sql_database.databases.present(
        ctx,
        **{
            "name": RESOURCE_NAME,
            "resource_group_name": RESOURCE_GROUP_NAME,
            "server_name": SERVER_NAME,
            "database_name": DATABASE_NAME,
        },
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Created azure.sql_database.databases '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.skip(reason="Test will be disabled until module is refactored")
@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of SQL databases. When a resource exists, 'present' should update the resource with patchable parameters.
    """
    mock_hub.states.azure.sql_database.databases.present = (
        hub.states.azure.sql_database.databases.present
    )
    mock_hub.tool.azure.sql_database.databases.convert_raw_database_to_present = (
        hub.tool.azure.sql_database.databases.convert_raw_database_to_present
    )
    mock_hub.tool.azure.sql_database.databases.convert_present_to_raw_database = (
        hub.tool.azure.sql_database.databases.convert_present_to_raw_database
    )
    mock_hub.tool.azure.sql_database.databases.update_database_payload = (
        hub.tool.azure.sql_database.databases.update_database_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.sql_database.databases.get = (
        hub.exec.azure.sql_database.databases.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.resource_utils.construct_resource_id = (
        hub.tool.azure.resource_utils.construct_resource_id
    )

    resource_parameters_update = {
        "location": "westeurope",
        "subscription_id": SUBSCRIPTION_ID,
        "sku": {
            "name": "Standard",
            "tier": "Standard",
            "capacity": 15,
        },
        "collation": "SQL_Latin1_General_CP1_CI_AS",
        "max_size_bytes": 268435456000,
        "read_scale": "Enabled",
        "requested_backup_storage_redundancy": "Local",
        "tags": {"tag-key-updated": "tag-value-updated"},
    }

    resource_parameters_update_raw = {
        "location": "westeurope",
        "properties": {
            "collation": "SQL_Latin1_General_CP1_CI_AS",
            "maxSizeBytes": 268435456000,
            "readScale": "Enabled",
            "requestedBackupStorageRedundancy": "Local",
        },
        "sku": {
            "name": "Standard",
            "tier": "Standard",
            "capacity": 15,
        },
        "tags": {"tag-key-updated": "tag-value-updated"},
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}/providers/Microsoft.Sql/servers/{SERVER_NAME}/databases/{DATABASE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}/providers/Microsoft.Sql/servers/{SERVER_NAME}/databases/{DATABASE_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert SERVER_NAME in url
        assert DATABASE_NAME in url
        assert resource_parameters_update_raw["properties"] == json.get("properties")
        assert resource_parameters_update_raw["location"] == json.get("location")
        assert resource_parameters_update_raw["tags"] == json.get("tags")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.sql_database.databases.present(
        test_ctx,
        **{
            "name": RESOURCE_NAME,
            "resource_group_name": RESOURCE_GROUP_NAME,
            "server_name": SERVER_NAME,
            "database_name": DATABASE_NAME,
        },
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.sql_database.databases '{RESOURCE_NAME}'" in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.sql_database.databases.present(
        ctx,
        **{
            "name": RESOURCE_NAME,
            "resource_group_name": RESOURCE_GROUP_NAME,
            "server_name": SERVER_NAME,
            "database_name": DATABASE_NAME,
        },
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.skip(reason="Test will be disabled until module is refactored")
@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of SQL database. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.sql_database.databases.absent = (
        hub.states.azure.sql_database.databases.absent
    )
    mock_hub.tool.azure.sql_database.databases.convert_raw_database_to_present = (
        hub.tool.azure.sql_database.databases.convert_raw_database_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.sql_database.databases.get = (
        hub.exec.azure.sql_database.databases.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.resource_utils.construct_resource_id = (
        hub.tool.azure.resource_utils.construct_resource_id
    )
    mock_hub.tool.azure.resource_utils.construct_resource_url = (
        hub.tool.azure.resource_utils.construct_resource_url
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert SERVER_NAME in url
        assert DATABASE_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.sql_database.databases.absent(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        SERVER_NAME,
        DATABASE_NAME,
        SUBSCRIPTION_ID,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.sql_database.databases '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.skip(reason="Test will be disabled until module is refactored")
@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of SQL databases.
    """
    mock_hub.states.azure.sql_database.databases.describe = (
        hub.states.azure.sql_database.databases.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.sql_database.databases.convert_raw_database_to_present = (
        hub.tool.azure.sql_database.databases.convert_raw_database_to_present
    )
    mock_hub.exec.azure.sql_database.databases.list = (
        hub.exec.azure.sql_database.databases.list
    )
    mock_hub.tool.azure.resource_utils.construct_resource_id = (
        hub.tool.azure.resource_utils.construct_resource_id
    )

    resource_id = f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}/providers/Microsoft.Sql/servers/{SERVER_NAME}/databases/{DATABASE_NAME}"
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.sql_database.databases.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.sql_database.databases.present" in ret_value.keys()
    described_resource = ret_value.get("azure.sql_database.databases.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert RESOURCE_NAME == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.skip(reason="Test will be disabled until module is refactored")
@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of SQL database. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.sql_database.databases.absent = (
        hub.states.azure.sql_database.databases.absent
    )
    mock_hub.tool.azure.sql_database.databases.convert_raw_database_to_present = (
        hub.tool.azure.sql_database.databases.convert_raw_database_to_present
    )
    mock_hub.tool.azure.sql_database.databases.convert_raw_sku_to_present = (
        hub.tool.azure.sql_database.databases.convert_raw_sku_to_present
    )
    mock_hub.tool.azure.sql_database.databases.convert_raw_identity_to_present = (
        hub.tool.azure.sql_database.databases.convert_raw_identity_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.sql_database.databases.get = (
        hub.exec.azure.sql_database.databases.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}/providers/Microsoft.Sql/servers/{SERVER_NAME}/databases/{DATABASE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert SERVER_NAME in url
        assert DATABASE_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.sql_database.databases.absent(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        SERVER_NAME,
        DATABASE_NAME,
        SUBSCRIPTION_ID,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.sql_database.databases '{RESOURCE_NAME}'" in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.sql_database.databases.absent(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        SERVER_NAME,
        DATABASE_NAME,
        SUBSCRIPTION_ID,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert f"Deleted azure.sql_database.databases '{RESOURCE_NAME}'" in ret["comment"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert SERVER_NAME == old_state.get("server_name")
        assert DATABASE_NAME == old_state.get("database_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["tags"] == old_state.get("tags")
        assert expected_old_state["subscription_id"] == old_state.get("subscription_id")
    if new_state:
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert SERVER_NAME == new_state.get("server_name")
        assert DATABASE_NAME == new_state.get("database_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["tags"] == new_state.get("tags")
        assert expected_new_state["subscription_id"] == new_state.get("subscription_id")
