import copy
from collections import ChainMap

import pytest


RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
ACCOUNT_NAME = "my-storage-account"
RESOURCE_PARAMETERS = {
    "location": "eastus",
    "sku_tier": "Standard",
    "sku_name": "Standard_LRS",
    "account_kind": "BlockBlobStorage",
    "is_hns_enabled": True,
    "nfsv3_enabled": True,
    "enable_https_traffic_only": False,
    "min_tls_version": "TLS1_2",
    "allow_blob_public_access": True,
    "allow_shared_key_access": True,
    "routing": {
        "publish_microsoft_endpoints": False,
        "publish_internet_endpoints": True,
        "routing_choice": "InternetRouting",
    },
    "encryption_service": {
        "encryption_key_source": "Microsoft.Keyvault",
        "queue_encryption_key_type": "Account",
        "table_encryption_key_type": "Service",
        "blob_encryption_key_type": "Account",
        "file_encryption_key_type": "Account",
    },
    "customer_managed_key": {
        "key_vault_uri": "myvault8569.vault.azure.net",
        "key_name": "wrappingKey",
        "key_version": "",
        "federated_identity_client_id": "f83c6b1b-4d34-47e4-bb34-9d83df58b540",
        "user_assigned_identity_id": "/subscriptions/{subscription-id}/resourceGroups/res9101/providers/Microsoft.ManagedIdentity/userAssignedIdentities/{managed-identity-name}",
    },
    "key_policy": {"key_expiration_period_in_days": 5},
    "azure_files_authentication": {
        "directory_service_options": "AADDS",
        "active_directory_properties": {
            "domain_guid": "asdadasd23423r2ff2gg2",
            "domain_name": "fdsfsd.com",
            "azure_storage_sid": "sdfsdfsgdfgh",
            "domain_sid": "fggdghrghrh",
            "forest_name": "adforest",
            "netbios_domain_name": "test.com",
        },
    },
}
RESOURCE_PARAMETERS_RAW = {
    "sku": {
        "name": "Standard_LRS",
        "tier": "Standard",
    },
    "kind": "BlockBlobStorage",
    "location": "eastus",
    "properties": {
        "isHnsEnabled": True,
        "isNfsV3Enabled": True,
        "supportsHttpsTrafficOnly": False,
        "minimumTlsVersion": "TLS1_2",
        "allowBlobPublicAccess": True,
        "allowSharedKeyAccess": True,
        "routingPreference": {
            "publishMicrosoftEndpoints": False,
            "publishInternetEndpoints": True,
            "routingChoice": "InternetRouting",
        },
        "encryption": {
            "services": {
                "queue": {"keyType": "Account"},
                "table": {"keyType": "Service"},
                "blob": {"keyType": "Account"},
                "file": {"keyType": "Account"},
            },
            "keyvaultproperties": {
                "keyvaulturi": "myvault8569.vault.azure.net",
                "keyname": "wrappingKey",
                "keyversion": "",
            },
            "keySource": "Microsoft.Keyvault",
            "identity": {
                "userAssignedIdentity": "/subscriptions/{subscription-id}/resourceGroups/res9101/providers/Microsoft.ManagedIdentity/userAssignedIdentities/{managed-identity-name}",
                "federatedIdentityClientId": "f83c6b1b-4d34-47e4-bb34-9d83df58b540",
            },
        },
        "keyPolicy": {"keyExpirationPeriodInDays": 5},
        "azureFilesIdentityBasedAuthentication": {
            "directoryServiceOptions": "AADDS",
            "activeDirectoryProperties": {
                "domainGuid": "asdadasd23423r2ff2gg2",
                "domainName": "fdsfsd.com",
                "azureStorageSid": "sdfsdfsgdfgh",
                "domainSid": "fggdghrghrh",
                "forestName": "adforest",
                "netBiosDomainName": "test.com",
            },
        },
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of storage account. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.storage_resource_provider.storage_accounts.present = (
        hub.states.azure.storage_resource_provider.storage_accounts.present
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )
    mock_hub.exec.azure.storage_resource_provider.storage_accounts.get = (
        hub.exec.azure.storage_resource_provider.storage_accounts.get
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_present_to_raw_storage_accounts = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_present_to_raw_storage_accounts
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.update_storage_accounts_payload = (
        hub.tool.azure.storage_resource_provider.storage_accounts.update_storage_accounts_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.storage_resource_provider.storage_accounts",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        assert json == RESOURCE_PARAMETERS_RAW
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = (
        await mock_hub.states.azure.storage_resource_provider.storage_accounts.present(
            test_ctx,
            RESOURCE_NAME,
            RESOURCE_GROUP_NAME,
            ACCOUNT_NAME,
            **RESOURCE_PARAMETERS,
        )
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = (
        await mock_hub.states.azure.storage_resource_provider.storage_accounts.present(
            ctx,
            RESOURCE_NAME,
            RESOURCE_GROUP_NAME,
            ACCOUNT_NAME,
            **RESOURCE_PARAMETERS,
        )
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of storage account. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.storage_resource_provider.storage_accounts.present = (
        hub.states.azure.storage_resource_provider.storage_accounts.present
    )
    mock_hub.exec.azure.storage_resource_provider.storage_accounts.get = (
        hub.exec.azure.storage_resource_provider.storage_accounts.get
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_present_to_raw_storage_accounts = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_present_to_raw_storage_accounts
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.update_storage_accounts_payload = (
        hub.tool.azure.storage_resource_provider.storage_accounts.update_storage_accounts_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    resource_parameters_update = {
        "location": "eastus",
        "sku_tier": "Standard",
        "sku_name": "Standard_LRS",
        "account_kind": "BlockBlobStorage",
        "is_hns_enabled": True,
        "nfsv3_enabled": True,
        "enable_https_traffic_only": True,
        "min_tls_version": "TLS1_1",
        "allow_blob_public_access": False,
        "allow_shared_key_access": False,
        "routing": {
            "publish_microsoft_endpoints": True,
            "publish_internet_endpoints": False,
            "routing_choice": "InternetRouting",
        },
        "encryption_service": {
            "encryption_key_source": "Microsoft.Keyvault",
            "queue_encryption_key_type": "Account",
            "table_encryption_key_type": "Service",
            "blob_encryption_key_type": "Account",
            "file_encryption_key_type": "Account",
        },
        "customer_managed_key": {
            "key_vault_uri": "myvault8569.vault.azure.net",
            "key_name": "wrappingKey",
            "key_version": "",
            "federated_identity_client_id": "f83c6b1b-4d34-47e4-bb34-9d83df58b540",
            "user_assigned_identity_id": "/subscriptions/{subscription-id}/resourceGroups/res9101/providers/Microsoft.ManagedIdentity/userAssignedIdentities/{managed-identity-name}",
        },
        "key_policy": {"key_expiration_period_in_days": 5},
        "azure_files_authentication": {
            "directory_service_options": "AADDS",
            "active_directory_properties": {
                "domain_guid": "asdadasd23423r2ff2gg2",
                "domain_name": "fdsfsd.com",
                "azure_storage_sid": "sdfsdfsgdfgh",
                "domain_sid": "fggdghrghrh",
                "forest_name": "adforest",
                "netbios_domain_name": "test.com",
            },
        },
    }
    resource_parameters_update_raw = {
        "sku": {
            "name": "Standard_LRS",
            "tier": "Standard",
        },
        "kind": "BlockBlobStorage",
        "location": "eastus",
        "properties": {
            "isHnsEnabled": True,
            "isNfsV3Enabled": True,
            "supportsHttpsTrafficOnly": True,
            "minimumTlsVersion": "TLS1_1",
            "allowBlobPublicAccess": False,
            "allowSharedKeyAccess": False,
            "routingPreference": {
                "publishMicrosoftEndpoints": True,
                "publishInternetEndpoints": False,
                "routingChoice": "InternetRouting",
            },
            "encryption": {
                "services": {
                    "queue": {"keyType": "Account"},
                    "table": {"keyType": "Service"},
                    "blob": {"keyType": "Account"},
                    "file": {"keyType": "Account"},
                },
                "keyvaultproperties": {
                    "keyvaulturi": "myvault8569.vault.azure.net",
                    "keyname": "wrappingKey",
                    "keyversion": "",
                },
                "keySource": "Microsoft.Keyvault",
                "identity": {
                    "userAssignedIdentity": "/subscriptions/{subscription-id}/resourceGroups/res9101/providers/Microsoft.ManagedIdentity/userAssignedIdentities/{managed-identity-name}",
                    "federatedIdentityClientId": "f83c6b1b-4d34-47e4-bb34-9d83df58b540",
                },
            },
            "keyPolicy": {"keyExpirationPeriodInDays": 5},
            "azureFilesIdentityBasedAuthentication": {
                "directoryServiceOptions": "AADDS",
                "activeDirectoryProperties": {
                    "domainGuid": "asdadasd23423r2ff2gg2",
                    "domainName": "fdsfsd.com",
                    "azureStorageSid": "sdfsdfsgdfgh",
                    "domainSid": "fggdghrghrh",
                    "forestName": "adforest",
                    "netBiosDomainName": "test.com",
                },
            },
        },
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        assert resource_parameters_update_raw["properties"] == json.get("properties")
        assert resource_parameters_update_raw["location"] == json.get("location")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = (
        await mock_hub.states.azure.storage_resource_provider.storage_accounts.present(
            test_ctx,
            RESOURCE_NAME,
            RESOURCE_GROUP_NAME,
            ACCOUNT_NAME,
            **resource_parameters_update,
        )
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = (
        await mock_hub.states.azure.storage_resource_provider.storage_accounts.present(
            ctx,
            RESOURCE_NAME,
            RESOURCE_GROUP_NAME,
            ACCOUNT_NAME,
            **resource_parameters_update,
        )
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of storage accounts. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.storage_resource_provider.storage_accounts.absent = (
        hub.states.azure.storage_resource_provider.storage_accounts.absent
    )
    mock_hub.exec.azure.storage_resource_provider.storage_accounts.get = (
        hub.exec.azure.storage_resource_provider.storage_accounts.get
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.storage_resource_provider.storage_accounts.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, ACCOUNT_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of storage account. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.storage_resource_provider.storage_accounts.absent = (
        hub.states.azure.storage_resource_provider.storage_accounts.absent
    )
    mock_hub.exec.azure.storage_resource_provider.storage_accounts.get = (
        hub.exec.azure.storage_resource_provider.storage_accounts.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.storage_resource_provider.storage_accounts.absent(
        test_ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, ACCOUNT_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.storage_resource_provider.storage_accounts.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, ACCOUNT_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of public-ip-address.
    """
    mock_hub.states.azure.storage_resource_provider.storage_accounts.describe = (
        hub.states.azure.storage_resource_provider.storage_accounts.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.exec.azure.storage_resource_provider.storage_accounts.list = (
        hub.exec.azure.storage_resource_provider.storage_accounts.list
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )

    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = (
        await mock_hub.states.azure.storage_resource_provider.storage_accounts.describe(
            ctx
        )
    )

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert (
        "azure.storage_resource_provider.storage_accounts.present" in ret_value.keys()
    )
    described_resource = ret_value.get(
        "azure.storage_resource_provider.storage_accounts.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        old_state_to_compare = copy.deepcopy(old_state)
        old_state_to_compare.pop("name", None)
        old_state_to_compare.pop("subscription_id", None)
        old_state_to_compare.pop("resource_id", None)

        expected_old_state_to_compare = copy.deepcopy(expected_old_state)
        expected_old_state_to_compare["resource_group_name"] = RESOURCE_GROUP_NAME
        expected_old_state_to_compare["account_name"] = ACCOUNT_NAME

        assert expected_old_state_to_compare == old_state_to_compare
    if new_state:
        new_state_to_compare = copy.deepcopy(new_state)
        new_state_to_compare.pop("name", None)
        new_state_to_compare.pop("subscription_id", None)
        new_state_to_compare.pop("resource_id", None)

        expected_new_state_to_compare = copy.deepcopy(expected_new_state)
        expected_new_state_to_compare["resource_group_name"] = RESOURCE_GROUP_NAME
        expected_new_state_to_compare["account_name"] = ACCOUNT_NAME

        assert expected_new_state_to_compare == new_state_to_compare
