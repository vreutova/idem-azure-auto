def test_construct_resource_id(hub):
    resource_id = hub.tool.azure.resource_utils.construct_resource_id(
        "compute.virtual_machines",
        {
            "resource_group_name": "rg_name",
            "virtual_machine_name": "vm_name",
            "subscription_id": "s_id",
        },
    )
    assert (
        resource_id
        == "/subscriptions/s_id/resourceGroups/rg_name/providers/Microsoft.Compute/virtualMachines/vm_name"
    )


def test_construct_resource_id_missing_values(hub):
    resource_id = hub.tool.azure.resource_utils.construct_resource_id(
        "compute.virtual_machines",
        {
            "resource_group_name": None,
            "virtual_machine_name": "vm_name",
            "subscription_id": "s_id",
        },
    )
    assert resource_id is None

    resource_id = hub.tool.azure.resource_utils.construct_resource_id(
        "compute.virtual_machines",
        {"virtual_machine_name": "vm_name", "subscription_id": "s_id"},
    )
    assert resource_id is None
