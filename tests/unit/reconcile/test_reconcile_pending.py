def test_reconcile_pending_result_true(hub, mock_hub, ctx):
    # Result 'True' and no 'changes' -> not pending
    ret = {"new_state": None, "old_state": None, "changes": None, "result": True}

    mock_hub.reconcile.pending.azure.is_pending = hub.reconcile.pending.azure.is_pending
    is_pending = mock_hub.reconcile.pending.azure.is_pending(ret)
    assert is_pending is False, is_pending


def test_reconcile_pending_result_false(hub, mock_hub, ctx):
    # Result 'False' and no 'changes' -> pending
    ret = {"new_state": None, "old_state": None, "changes": None, "result": False}

    mock_hub.reconcile.pending.azure.is_pending = hub.reconcile.pending.azure.is_pending
    is_pending = mock_hub.reconcile.pending.azure.is_pending(ret)
    assert is_pending is True, is_pending


def test_reconcile_pending_result_true_with_changes(hub, mock_hub, ctx):
    # Result 'True' and 'changes' -> pending
    ret = {
        "new_state": None,
        "old_state": None,
        "changes": {"new": "test-new"},
        "result": True,
    }

    mock_hub.reconcile.pending.azure.is_pending = hub.reconcile.pending.azure.is_pending
    is_pending = mock_hub.reconcile.pending.azure.is_pending(ret)
    assert is_pending is True, is_pending


def test_reconcile_pending_prov_state_updating(hub, mock_hub, ctx):
    # 'provisioningState' is 'Updating' -> pending
    ret = {
        "new_state": {
            "name": "test-subnet",
            "id": "/test-vnet/subnets/test-subnet",
            "properties": {"provisioningState": "Updating"},
        },
        "old_state": None,
        "changes": {},
        "result": True,
    }

    mock_hub.reconcile.pending.azure.is_pending = hub.reconcile.pending.azure.is_pending
    is_pending = mock_hub.reconcile.pending.azure.is_pending(ret)
    assert is_pending is True, is_pending


def test_reconcile_pending_prov_state_failed(hub, mock_hub, ctx):
    # 'provisioningState' is 'Failed' -> pending
    ret = {
        "new_state": {
            "name": "test-subnet",
            "id": "/test-vnet/subnets/test-subnet",
            "properties": {"provisioningState": "Failed"},
        },
        "old_state": None,
        "changes": {},
        "result": True,
    }

    mock_hub.reconcile.pending.azure.is_pending = hub.reconcile.pending.azure.is_pending
    is_pending = mock_hub.reconcile.pending.azure.is_pending(ret)
    assert is_pending is False, is_pending
