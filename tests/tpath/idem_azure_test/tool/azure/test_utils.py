import copy
import os
import pathlib
import uuid
from random import random
from typing import Any
from typing import Dict
from typing import List

import yaml
from pop.mods.pop.data import IMAP
from pytest_idem import runner


def create_sls_from_present_state_props(
    hub,
    resource_type: str,
    present_state_props: Dict[str, Any],
) -> str:
    name = present_state_props.get("name", "state_name")
    # yaml representer does not handle NamespaceDict class, clone it in order to have it as dict
    present_state_properties = copy.deepcopy(copy.copy(present_state_props))
    present_state_str = yaml.safe_dump(
        {
            name: {
                f"azure.{resource_type}.present": [
                    {k: v} for k, v in present_state_properties.items()
                ]
            }
        }
    )
    return present_state_str


def call_present_from_properties(
    hub,
    idem_cli,
    resource_type: str,
    present_state_properties: Dict,
    test: bool = False,
    additional_kwargs: List[str] = None,
    acct_profile: str = "test_development_idem_azure",
) -> Dict:
    present_state_str = hub.tool.azure.test_utils.create_sls_from_present_state_props(
        resource_type=resource_type, present_state_props=present_state_properties
    )

    present_state_ret = hub.tool.azure.test_utils.run_idem_state(
        idem_cli, present_state_str, test, additional_kwargs, acct_profile
    )
    return hub.tool.azure.test_utils.get_esm_tagged_data(
        present_state_ret, f"azure.{resource_type}"
    )


async def call_absent(
    hub,
    ctx,
    idem_cli,
    resource_type: str,
    name: str,
    resource_id: str = None,
    test: bool = False,
    absent_params: Dict[str, str] = None,
    additional_kwargs: List[str] = None,
    wait_for_absent: bool = False,
    acct_profile: str = "test_development_idem_azure",
) -> Dict:
    params_string = ""
    if absent_params:
        for absent_param, param_value in absent_params.items():
            params_string += " " * 14
            param_string = f"- {absent_param}: {param_value}"
            params_string += f"{param_string}\n"

    if resource_id:
        absent_state_str = f"""
            {name}:
              azure.{resource_type}.absent:
              - resource_id: {resource_id}"""
    else:
        absent_state_str = f"""
            {name}:
              azure.{resource_type}.absent:
              - name: {name}"""

    if params_string:
        absent_state_str = f"{absent_state_str}\n{params_string}"

    absent_state_ret = hub.tool.azure.test_utils.run_idem_state(
        idem_cli, absent_state_str, test, additional_kwargs, acct_profile
    )
    absent_state_ret = hub.tool.azure.test_utils.get_esm_tagged_data(
        absent_state_ret, f"azure.{resource_type}"
    )
    if wait_for_absent and absent_state_ret.get("old_state"):
        api_version = hub.tool.azure.api_versions.get_api_version(resource_type)
        # resource was deleted as a result of this call
        await hub.tool.azure.resource.wait_for_absent(
            ctx,
            url=f"{ctx.acct.endpoint_url}{resource_id}?api-version={api_version}",
            retry_count=15,
            retry_period=25,
        )
    return absent_state_ret


def run_idem_state(
    hub,
    idem_cli,
    state: str,
    test: bool = False,
    additional_args: List = None,
    acct_profile: str = "test_development_idem_azure",
):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(state)
        args = [f"--acct-profile={acct_profile}"]
        if additional_args:
            for item in additional_args:
                args.append(item)
        if test:
            args.append("--test")
        return idem_cli(
            "state",
            fh,
            *args,
            check=True,
        ).json


def run_state_with_hub(
    hub,
    resource_type: str,
    present_state_properties: Dict[str, Any],
    test: bool = False,
    acct_data: Dict[str, Any] = None,
):
    present_state_str = hub.tool.azure.test_utils.create_sls_from_present_state_props(
        resource_type=resource_type, present_state_props=present_state_properties
    )

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(present_state_str)
        result = hub.tool.azure.test_utils.run_sls_with_hub(
            sls_path=fh,
            test=test,
            acct_data=acct_data,
        )
        result = hub.tool.azure.test_utils.get_esm_tagged_data(
            result, f"azure.{resource_type}"
        )
        return result


def run_sls_with_hub(
    hub,
    # idem_cli,
    sls_path: pathlib.Path,
    acct_data: Dict[str, Any],
    test: bool = False,
):
    sls_file_name = sls_path.name
    sls_file_directory = str(sls_path.parent)

    run_sls_result = runner.run_sls(
        sls=[sls_file_name],
        sls_sources=[f"file://{sls_file_directory}"],
        acct_data=acct_data,
        test=test,
        hub=hub,
    )
    return run_sls_result


def set_get_resource_only_with_resource_id_flag_on_hub(hub, value: bool):
    idem_opt = copy.copy(hub.OPT.idem)
    idem_opt = {**idem_opt, "get_resource_only_with_resource_id": value}
    hub.OPT = IMAP(copy.copy(hub.OPT), idem=idem_opt)


def fake_login_on_hub(hub):
    original_post = hub.exec.request.json.post

    async def mock_post(
        ctx,
        url: str,
        **kwargs,
    ):
        if url.endswith("oauth2/token"):
            return {
                "result": True,
                "status": True,
                "ret": {"access_token": "fake_token"},
            }
        return original_post(hub, ctx, url=url, **kwargs)

    hub.exec.request.json.post = mock_post


def get_esm_tagged_data(hub, data: Dict, resource_type: str = None):
    ret = None
    found: bool = False
    for key, value in data.items():
        if "_|-" in key:
            if resource_type:
                res_type, _, _, _ = key.split("_|-", maxsplit=3)
                if res_type != resource_type:
                    continue
            if found:
                raise ValueError("Duplicate matching tags found!")
            ret = value
            found = True

    if not found:
        raise ValueError("No matching tag found!")

    return ret


def generate_unique_name(hub, name_prefix: str, max_length: int = 62) -> str:
    r"""Uses the CI_MERGE_REQUEST_IID and JOB_ID environment variable to generate unique name.
    This can help associate the resource with the pipeline that has created it. If the test is
    run locally the second part is missing:
        {name_prefix}{MERGE_REQUEST_IID-JOB_ID}?-{random-part}

    Args:
        name_prefix: name prefix between 2 and 42 characters.
        max_length: the maximum number of characters of the returned name. Default is 62.

    Returns:
        random name up to 62 characters in length. The returned name guaranteed will not end
        with '-' but with number or letter.

    Examples:
        generate_unique_name(None) -> ValueError("name is None")
        generate_unique_name('') -> ValueError("name is too short")
        generate_unique_name('this-is-a-very-long-name-prefix-0-1-2-3-4-5') -> ValueError("name is too long")

        standalone:  generate_unique_name('idem-test') -> idem-azure-3m2422oxdcygx87wvmo3pffkw
        standalone:  generate_unique_name('idem-test', 20) -> idem-azure-3m2422oxdcy
        in pipeline: generate_unique_name('idem-test') -> idem-test-123-3997117126-3m2422oxdcygx87wvmo3pffkw
    """

    if not name_prefix:
        raise ValueError("name is None")

    if len(name_prefix) < 2:
        raise ValueError("name is too short: " + name_prefix)

    if len(name_prefix) > 42:
        raise ValueError("name is too long: " + name_prefix)

    if max_length < 10:
        raise ValueError(
            "max_length bigger than 9 is required, current is: " + max_length
        )

    result = name_prefix + "-"

    merge_request_id = os.environ.get("CI_MERGE_REQUEST_IID")
    if merge_request_id:
        result += merge_request_id.lower() + "-"

    job_id = os.environ.get("CI_JOB_ID")
    if job_id:
        result += job_id.lower() + "-"

    chars = "0123456789abcdefghijklmnopqrstuvwxyz"
    result += __to_base(uuid.uuid4().int, 36, chars)
    result = result[:max_length]

    if result[-1] == "-":
        result = result[:-1] + random.choice(chars)

    return result


def check_actual_includes_expected(
    hub, actual: Any, expected: Any, ignore_props: List[str]
):
    if isinstance(expected, dict):
        assert isinstance(actual, dict), f"Expected dictionary, got {actual}"
        for prop, expected_value in expected.items():
            if prop in ignore_props:
                continue
            if expected_value is None:
                continue
            assert prop in actual, f"{prop} not in {actual}"
            ignored_prefix = f"{prop}."
            new_ignore_props = [
                ignored_prop[len(ignored_prefix) :]
                for ignored_prop in ignore_props
                if ignored_prop.startswith(ignored_prefix)
            ]
            hub.tool.azure.test_utils.check_actual_includes_expected(
                actual.get(prop), expected_value, new_ignore_props
            )
    elif isinstance(expected, list):
        assert isinstance(actual, list), f"Expected list, got {actual}"
        assert len(actual) == len(
            expected
        ), f"Expected lists to have equal lengths: {actual}, {expected}"
        new_ignore_props = [
            ignored_prop[3:]
            for ignored_prop in ignore_props
            if ignored_prop.startswith("[].")
        ]
        for index, list_item in enumerate(expected):
            hub.tool.azure.test_utils.check_actual_includes_expected(
                actual[index], list_item, new_ignore_props
            )
    else:
        assert actual == expected, f"Expected {expected}, got {actual}"


def _get_items_at_path_recurse(
    items_at_current_path: List[Any], remaining_path: List[str]
) -> List[Any]:
    if not remaining_path:
        return items_at_current_path
    nested_key = remaining_path[0]
    new_remaining_path = remaining_path[1:]
    is_list_value = nested_key.endswith("[]")
    if is_list_value:
        nested_key = nested_key[:-2]

    new_items = []
    for item in items_at_current_path:
        if not isinstance(item, dict):
            raise RuntimeError(
                f"Cannot get key {nested_key} in item {item} - not a dictionary"
            )
        nested_value = item.get(nested_key)
        if is_list_value and not isinstance(nested_value, list):
            raise RuntimeError(
                f"Value at key {nested_key} expected to be list but is {nested_value}"
            )
        if is_list_value:
            # nested_value is list
            new_items.extend(nested_value)
        else:
            new_items.append(nested_value)

    return _get_items_at_path_recurse(new_items, new_remaining_path)


def get_items_at_path(state: Dict[str, Any], path: str) -> List[Any]:
    wrapped_result = _get_items_at_path_recurse([state], path.split("."))
    if "[]" not in path:
        return wrapped_result[0]
    return wrapped_result


def compare_nested_paths(
    hub, state_1: Dict[str, Any], state_2: Dict[str, Any], path_1: str, path_2: str
) -> bool:
    return get_items_at_path(state_1, path_1) == get_items_at_path(state_2, path_2)


def get_nested_paths_comparator(hub, state_1: Dict[str, Any], state_2: Dict[str, Any]):
    return lambda path_1, path_2: hub.tool.azure.test_utils.compare_nested_paths(
        state_1, state_2, path_1, path_2
    )


def get_nested_paths_equality_asserter(
    hub, state_1: Dict[str, Any], state_2: Dict[str, Any]
):
    comparator = hub.tool.azure.test_utils.get_nested_paths_comparator(state_1, state_2)

    def return_function(path_1, path_2):
        assert comparator(path_1, path_2) is True

    return return_function


def __to_base(s, b, chars):
    res = ""
    while s:
        res += chars[s % b]
        s //= b
    return res[::-1] or "0"


def check_both_returned_states(
    hub,
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    name,
    resource_id,
    resource_id_properties: Dict = None,
):
    check_returned_states(
        hub,
        old_state,
        expected_old_state,
        name,
        resource_id,
        resource_id_properties=resource_id_properties,
    )
    check_returned_states(
        hub,
        new_state,
        expected_new_state,
        name,
        resource_id,
        resource_id_properties=resource_id_properties,
    )


def check_returned_states(
    hub,
    actual_state,
    expected_state,
    name,
    resource_id,
    resource_id_properties: Dict = None,
):
    if actual_state:
        if resource_id_properties:
            for prop_name, prop_val in resource_id_properties.items():
                assert actual_state.get(prop_name) == prop_val
        assert actual_state.get("name") == name
        assert actual_state.get("resource_id") == resource_id

    hub.tool.azure.test_utils.check_actual_includes_expected(
        actual_state, expected_state, []
    )
