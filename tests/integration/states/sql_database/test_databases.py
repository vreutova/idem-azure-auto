import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_sql_database_full(hub, ctx, idem_cli):
    """
    This test provisions a SQL database, describes databases, does a force update and deletes the provisioned SQL database.
    """
    unique_str = str(uuid.uuid4())
    # Create SQL database
    resource_group_name = "default-rg"
    database_name = "idem-test-database-" + unique_str
    server_name = "parallel-preallocate-test"
    database_parameters = {
        "location": "westeurope",
        "subscription_id": ctx.acct.subscription_id,
        "sku": {
            "name": "GP_Gen5",
            "tier": "GeneralPurpose",
            "family": "Gen5",
            "capacity": 2,
        },
        "collation": "SQL_Latin1_General_CP1_CI_AS",
        "max_size_bytes": 17179869184,
        "read_scale": "Disabled",
        "requested_backup_storage_redundancy": "Local",
        "tags": {
            f"idem-test-tag-key-" + unique_str: f"idem-test-tag-value-" + unique_str
        },
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create SQL database with --test
    database_ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli,
        "sql_database.databases",
        {
            "name": database_name,
            "resource_group_name": resource_group_name,
            "server_name": server_name,
            "database_name": database_name,
            **database_parameters,
        },
        True,
    )
    assert database_ret["result"], database_ret["comment"]
    assert not database_ret["old_state"] and database_ret["new_state"]
    assert (
        f"Would create azure.sql_database.databases '{database_name}'"
        in database_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=database_ret["new_state"],
        expected_old_state=None,
        expected_new_state=database_parameters,
        resource_group_name=resource_group_name,
        server_name=server_name,
        database_name=database_name,
        idem_resource_name=database_name,
    )
    resource_id = database_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Sql/servers/{server_name}/databases/{database_name}"
        == resource_id
    )

    # Create SQL database in real
    database_ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli,
        "sql_database.databases",
        {
            "name": database_name,
            "resource_group_name": resource_group_name,
            "server_name": server_name,
            "database_name": database_name,
            **database_parameters,
        },
        False,
    )
    assert database_ret["result"], database_ret["comment"]
    assert not database_ret["old_state"] and database_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=database_ret["new_state"],
        expected_old_state=None,
        expected_new_state=database_parameters,
        resource_group_name=resource_group_name,
        server_name=server_name,
        database_name=database_name,
        idem_resource_name=database_name,
    )
    resource_id = database_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Sql/servers/{server_name}/databases/{database_name}"
        == resource_id
    )

    # Describe SQL database
    describe_ret = await hub.states.azure.sql_database.databases.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get("azure.sql_database.databases.present")
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        server_name=server_name,
        database_name=database_name,
        resource_group_name=resource_group_name,
        idem_resource_name=database_name,
    )

    unique_str2 = str(uuid.uuid4())
    database_update_parameters = {
        "location": "westeurope",
        "subscription_id": ctx.acct.subscription_id,
        "sku": {
            "name": "GP_Gen5",
            "tier": "GeneralPurpose",
            "family": "Gen5",
            "capacity": 2,
        },
        "collation": "SQL_Latin1_General_CP1_CI_AS",
        "max_size_bytes": 34359738368,
        "read_scale": "Disabled",
        "requested_backup_storage_redundancy": "Local",
        "tags": {
            f"idem-test-tag-key-" + unique_str2: f"idem-test-tag-value-" + unique_str2
        },
    }
    # Update SQL database with --test
    database_ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli,
        "sql_database.databases",
        {
            "name": database_name,
            "resource_group_name": resource_group_name,
            "server_name": server_name,
            "database_name": database_name,
            **database_update_parameters,
        },
        True,
    )
    assert database_ret["result"], database_ret["comment"]
    assert database_ret["old_state"] and database_ret["new_state"]
    assert (
        f"Would update azure.sql_database.databases '{database_name}'"
        in database_ret["comment"]
    )
    check_returned_states(
        old_state=database_ret["old_state"],
        new_state=database_ret["new_state"],
        expected_old_state=database_parameters,
        expected_new_state=database_update_parameters,
        resource_group_name=resource_group_name,
        server_name=server_name,
        database_name=database_name,
        idem_resource_name=database_name,
    )
    resource_id = database_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Sql/servers/{server_name}/databases/{database_name}"
        == resource_id
    )

    # Update SQL database in real
    database_ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli,
        "sql_database.databases",
        {
            "name": database_name,
            "resource_group_name": resource_group_name,
            "server_name": server_name,
            "database_name": database_name,
            **database_update_parameters,
        },
        False,
    )
    assert database_ret["result"], database_ret["comment"]
    assert database_ret["old_state"] and database_ret["new_state"]
    assert (
        f"Updated azure.sql_database.databases '{database_name}'"
        in database_ret["comment"]
    )
    check_returned_states(
        old_state=database_ret["old_state"],
        new_state=database_ret["new_state"],
        expected_old_state=database_parameters,
        expected_new_state=database_update_parameters,
        resource_group_name=resource_group_name,
        server_name=server_name,
        database_name=database_name,
        idem_resource_name=database_name,
    )
    resource_id = database_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Sql/servers/{server_name}/databases/{database_name}"
        == resource_id
    )

    # Delete SQL database with --test
    database_del_ret = await hub.tool.azure.test_utils.call_absent(
        ctx,
        idem_cli,
        "sql_database.databases",
        database_name,
        resource_id,
        True,
        absent_params={
            "resource_group_name": resource_group_name,
            "server_name": server_name,
            "database_name": database_name,
            "subscription_id": ctx.acct.subscription_id,
        },
    )
    assert database_del_ret["result"], database_del_ret["comment"]
    assert database_del_ret["old_state"] and not database_del_ret["new_state"]
    assert (
        f"Would delete azure.sql_database.databases '{database_name}'"
        in database_del_ret["comment"]
    )
    check_returned_states(
        old_state=database_del_ret["old_state"],
        new_state=None,
        expected_old_state=database_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        server_name=server_name,
        database_name=database_name,
        idem_resource_name=database_name,
    )

    # Delete SQL database in real
    database_del_ret = await hub.tool.azure.test_utils.call_absent(
        ctx,
        idem_cli,
        "sql_database.databases",
        database_name,
        resource_id,
        False,
        absent_params={
            "resource_group_name": resource_group_name,
            "server_name": server_name,
            "database_name": database_name,
            "subscription_id": ctx.acct.subscription_id,
        },
    )
    assert database_del_ret["result"], database_del_ret["comment"]
    assert database_del_ret["old_state"] and not database_del_ret["new_state"]
    assert (
        f"Deleted azure.sql_database.databases '{database_name}'"
        in database_del_ret["comment"]
    )
    check_returned_states(
        old_state=database_del_ret["old_state"],
        new_state=None,
        expected_old_state=database_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        server_name=server_name,
        database_name=database_name,
        idem_resource_name=database_name,
    )

    # Delete SQL database again
    database_del_ret = await hub.tool.azure.test_utils.call_absent(
        ctx,
        idem_cli,
        "sql_database.databases",
        database_name,
        resource_id,
        True,
        absent_params={
            "resource_group_name": resource_group_name,
            "server_name": server_name,
            "database_name": database_name,
            "subscription_id": ctx.acct.subscription_id,
        },
    )
    assert database_del_ret["result"], database_del_ret["comment"]
    assert not database_del_ret["old_state"] and not database_del_ret["new_state"]
    assert (
        f"azure.sql_database.databases '{database_name}' already absent"
        in database_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    server_name,
    database_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert server_name == old_state.get("server_name")
        assert database_name == old_state.get("database_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["sku"] == old_state.get("sku")
        assert expected_old_state["collation"] == old_state.get("collation")
        assert expected_old_state["max_size_bytes"] == old_state.get("max_size_bytes")
        assert expected_old_state["read_scale"] == old_state.get("read_scale")
        assert expected_old_state[
            "requested_backup_storage_redundancy"
        ] == old_state.get("requested_backup_storage_redundancy")
        assert expected_old_state["tags"] == old_state.get("tags")
        assert expected_old_state["subscription_id"] == old_state.get("subscription_id")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert server_name == new_state.get("server_name")
        assert database_name == new_state.get("database_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["sku"] == new_state.get("sku")
        assert expected_new_state["collation"] == new_state.get("collation")
        assert expected_new_state["max_size_bytes"] == new_state.get("max_size_bytes")
        assert expected_new_state["read_scale"] == new_state.get("read_scale")
        assert expected_new_state[
            "requested_backup_storage_redundancy"
        ] == new_state.get("requested_backup_storage_redundancy")
        assert expected_new_state["tags"] == new_state.get("tags")
        assert expected_new_state["subscription_id"] == new_state.get("subscription_id")
