import copy
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
RESOURCE_TYPE = "compute.disks"
RESOURCE_ID_FORMAT = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Compute/disks/{disk_name}"

PROPERTIES = {
    "location": "westus",
    "creation_data": {
        "create_option": "FromImage",
        "image_reference": {"id": ""},
    },
    "disk_size_gb": 32,
}

UPDATED_PROPERTIES = {
    "disk_size_gb": 40,
}


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_create")
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_disks_create(
    hub,
    ctx,
    idem_cli,
    __test,
    resource_group_fixture,
    module_cleaner,
):
    name = hub.tool.azure.test_utils.generate_unique_name("idem-test-disk")
    PROPERTIES["name"] = name
    PROPERTIES["disk_name"] = name
    subscription_id = ctx.acct.get("subscription_id")
    PROPERTIES["subscription_id"] = subscription_id
    PROPERTIES["resource_group_name"] = resource_group_fixture.get("name")
    PROPERTIES["creation_data"]["image_reference"][
        "id"
    ] = f"/Subscriptions/{subscription_id}/Providers/Microsoft.Compute/Locations/westus/Publishers/Canonical/ArtifactTypes/VMImage/Offers/UbuntuServer/Skus/16.04-LTS/Versions/16.04.202109280"

    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PROPERTIES, __test
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_create_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=name,
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.create_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=name,
            )
            in ret["comment"]
        )
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)
    expected_state = {"resource_id": resource_id, **PROPERTIES}
    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["new_state"], expected_state, []
    )
    module_cleaner.mark_for_deletion(ret["new_state"], RESOURCE_TYPE)


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present_create"])
async def test_disks_describe(hub, ctx):
    ret = await hub.states.azure.compute.disks.describe(ctx)
    current_resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)
    for resource_id in ret:
        described_resource = ret[resource_id].get(f"azure.{RESOURCE_TYPE}.present")
        assert described_resource
        resource_state = dict(ChainMap(*described_resource))

        if resource_id == current_resource_id:
            expected_state = {"resource_id": resource_id, **PROPERTIES}
            expected_state["name"] = resource_id
            assert resource_state == expected_state
        else:
            assert resource_state.get("resource_id") == resource_id


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_update", depends=["present_create"])
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_disks_empty_update(hub, ctx, idem_cli, __test):
    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PROPERTIES, __test
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.no_property_to_be_updated_comment(
            resource_type=f"azure.{RESOURCE_TYPE}",
            name=PROPERTIES["name"],
        )
        in ret["comment"]
    )
    assert ret["old_state"] == ret["new_state"]
    expected_new_state = {
        "resource_id": RESOURCE_ID_FORMAT.format(**PROPERTIES),
        **PROPERTIES,
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["new_state"], expected_new_state, []
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_update", depends=["present_create"])
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_disks_update(hub, ctx, idem_cli, __test):
    updated_state = copy.deepcopy(PROPERTIES)
    updated_state.update(UPDATED_PROPERTIES)
    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, updated_state, __test
    )

    assert ret["result"], ret["comment"]
    resource_id = RESOURCE_ID_FORMAT.format(**updated_state)
    expected_old_state = {"resource_id": resource_id, **PROPERTIES}
    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["old_state"], expected_old_state, []
    )

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_update_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=updated_state["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.update_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=updated_state["name"],
            )
            in ret["comment"]
        )
    expected_new_state = {"resource_id": resource_id, **updated_state}
    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["new_state"], expected_new_state, []
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="delete", depends=["present_update"])
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_disks_absent(hub, ctx, idem_cli, __test, module_cleaner):
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)

    ret = await hub.tool.azure.test_utils.call_absent(
        ctx,
        idem_cli,
        RESOURCE_TYPE,
        PROPERTIES["name"],
        resource_id,
        test=__test,
        absent_params={
            "resource_group_name": PROPERTIES["resource_group_name"],
            "disk_name": PROPERTIES["disk_name"],
        },
    )
    module_cleaner.mark_deleted(resource_id)

    assert ret["result"], ret["comment"]
    current_state = copy.deepcopy(PROPERTIES)
    current_state.update(UPDATED_PROPERTIES)
    expected_old_state = {"resource_id": resource_id, **current_state}
    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["old_state"], expected_old_state, []
    )
    assert not ret["new_state"]

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_delete_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=PROPERTIES["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.delete_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=PROPERTIES["name"],
            )
            in ret["comment"]
        )
        ret = await hub.exec.azure.compute.disks.get(ctx, resource_id=resource_id)
        assert ret["result"]
        assert not ret["ret"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_disks_already_absent(hub, ctx, idem_cli, __test):
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)

    ret = await hub.tool.azure.test_utils.call_absent(
        ctx,
        idem_cli,
        RESOURCE_TYPE,
        PROPERTIES["name"],
        resource_id,
        test=__test,
        absent_params={
            "resource_group_name": PROPERTIES["resource_group_name"],
            "disk_name": PROPERTIES["disk_name"],
        },
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]

    assert (
        hub.tool.azure.comment_utils.already_absent_comment(
            resource_type=f"azure.{RESOURCE_TYPE}",
            name=PROPERTIES["name"],
        )
        in ret["comment"]
    )
    ret = await hub.exec.azure.compute.disks.get(ctx, resource_id=resource_id)
    assert ret["result"]
    assert not ret["ret"]
