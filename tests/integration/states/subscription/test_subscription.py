import copy
import time

import pytest


@pytest.mark.asyncio
async def test_subscription(hub, ctx):
    """
    This test provisions a subscription, describes subscriptions and deletes the provisioned subscription.
    """
    # Create subscription
    subscription_alias = "idem-new-test-subscription-alias-" + str(int(time.time()))
    display_name = "integration-new-test-subscription"
    workload = "Production"
    sub_parameters = {
        "billing_scope": "/providers/Microsoft.Billing/billingAccounts/55263077/enrollmentAccounts/303385",
        "display_name": display_name,
        "workload": workload,
    }

    resource_parameters = {
        "alias": subscription_alias,
        "display_name": display_name,
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create subscription with --test
    sub_ret = await hub.states.azure.subscription.subscriptions.present(
        test_ctx,
        name=subscription_alias,
        alias=subscription_alias,
        **sub_parameters,
    )
    assert sub_ret["result"], sub_ret["comment"]
    assert not sub_ret["old_state"] and sub_ret["new_state"]
    assert (
        f"Would create azure.subscription.subscriptions '{subscription_alias}'"
        in sub_ret["comment"]
    )
    assert (
        sub_ret["new_state"]["subscription_id"]
        and sub_ret["new_state"]["subscription_id"]
        == "subscription_id_known_after_present"
    )
    check_returned_states(
        old_state=None,
        new_state=sub_ret["new_state"],
        expected_old_state=None,
        expected_new_state=resource_parameters,
        idem_resource_name=subscription_alias,
    )
    resource_id = sub_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Subscription/aliases/{subscription_alias}" == resource_id
    )

    # Create subscription in real
    sub_ret = await hub.states.azure.subscription.subscriptions.present(
        ctx,
        name=subscription_alias,
        alias=subscription_alias,
        **sub_parameters,
    )
    assert sub_ret["result"], sub_ret["comment"]
    assert not sub_ret["old_state"] and sub_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=sub_ret["new_state"],
        expected_old_state=None,
        expected_new_state=resource_parameters,
        idem_resource_name=subscription_alias,
    )
    resource_id = sub_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Subscription/aliases/{subscription_alias}" == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2020-09-01",
        retry_count=5,
        retry_period=10,
    )

    sub_update_parameters = {
        "billing_scope": "/providers/Microsoft.Billing/billingAccounts/55263077/enrollmentAccounts/303385",
        "display_name": "integration-test-subscription-updated",
        "workload": workload,
    }

    sub_updated_raw_parameters = {
        "alias": subscription_alias,
        "display_name": "integration-test-subscription-updated",
    }

    # Update subscription with --test
    sub_ret = await hub.states.azure.subscription.subscriptions.present(
        test_ctx,
        name=subscription_alias,
        alias=subscription_alias,
        **sub_update_parameters,
    )
    assert sub_ret["result"], sub_ret["comment"]
    assert sub_ret["old_state"] and sub_ret["new_state"]
    assert (
        f"Would update azure.subscription.subscriptions '{subscription_alias}'"
        in sub_ret["comment"]
    )
    check_returned_states(
        old_state=sub_ret["old_state"],
        new_state=sub_ret["new_state"],
        expected_old_state=resource_parameters,
        expected_new_state=sub_updated_raw_parameters,
        idem_resource_name=subscription_alias,
    )
    resource_id = sub_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Subscription/aliases/{subscription_alias}" == resource_id
    )

    # Update subscription in real
    sub_ret = await hub.states.azure.subscription.subscriptions.present(
        ctx,
        name=subscription_alias,
        alias=subscription_alias,
        **sub_update_parameters,
    )
    assert sub_ret["result"], sub_ret["comment"]
    assert sub_ret["old_state"] and sub_ret["new_state"]
    assert (
        f"Updated azure.subscription.subscriptions '{subscription_alias}'"
        in sub_ret["comment"]
    )
    check_returned_states(
        old_state=sub_ret["old_state"],
        new_state=sub_ret["new_state"],
        expected_old_state=resource_parameters,
        expected_new_state=sub_updated_raw_parameters,
        idem_resource_name=subscription_alias,
    )
    resource_id = sub_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Subscription/aliases/{subscription_alias}" == resource_id
    )

    # Delete subscription with --test
    sub_del_ret = await hub.states.azure.subscription.subscriptions.absent(
        test_ctx,
        name=subscription_alias,
        alias=subscription_alias,
    )
    assert sub_del_ret["result"], sub_del_ret["comment"]
    assert sub_del_ret["old_state"] and not sub_del_ret["new_state"]
    assert (
        f"Would delete azure.subscription.subscriptions '{subscription_alias}'"
        in sub_del_ret["comment"]
    )

    # Delete Subscription
    sub_del_ret = await hub.states.azure.subscription.subscriptions.absent(
        ctx, name=subscription_alias, alias=subscription_alias
    )
    assert sub_del_ret["result"], sub_del_ret["comment"]
    assert sub_del_ret["old_state"] and not sub_del_ret["new_state"]
    assert (
        f"Deleted azure.subscription.subscriptions '{subscription_alias}'"
        in sub_del_ret["comment"]
    )
    check_returned_states(
        old_state=sub_ret["old_state"],
        new_state=None,
        expected_old_state=resource_parameters,
        expected_new_state=None,
        idem_resource_name=subscription_alias,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2020-09-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete subscription again
    sub_del_ret = await hub.states.azure.subscription.subscriptions.absent(
        ctx,
        name=subscription_alias,
        alias=subscription_alias,
    )
    assert sub_del_ret["result"], sub_del_ret["comment"]
    assert not sub_del_ret["old_state"] and not sub_del_ret["new_state"]
    assert (
        f"azure.subscriptions.subscription '{subscription_alias}' already absent"
        in sub_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert expected_old_state["alias"] == old_state.get("alias")
        assert expected_old_state["display_name"] == old_state.get("display_name")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert expected_new_state["alias"] == new_state.get("alias")
        assert expected_new_state["display_name"] == new_state.get("display_name")
